package com.example.starter;

import com.example.starter.vertx.EventBusVerticle;
import com.example.starter.vertx.MainVerticle;
import io.vertx.core.Verticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;

public class EntryPoint {
  public static void main(String[] args) {
    Vertx vertx = Vertx.vertx();
    Verticle mainVerticle = new MainVerticle();
    vertx.deployVerticle(mainVerticle);
    System.out.println("Entry Point Started");
  }
}
