package com.example.starter.vertx;

import com.example.starter.temporal.definitions.EventbusAddresses;
import com.example.starter.temporal.definitions.Shared;
import com.example.starter.temporal.models.Rates;
import com.example.starter.temporal.workflows.TemporalHelloWoldWorkflow;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.temporal.client.WorkflowClient;
import io.temporal.client.WorkflowOptions;
import io.temporal.serviceclient.WorkflowServiceStubs;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class HttpServerVerticle extends AbstractVerticle {
  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    //create router
    Router router = Router.router(vertx);
    //define router body
    router.route().handler(BodyHandler.create());

    router.post("/greeting").handler(this::greeting);
    router.post("/startWorkFlow").handler(this::getRate);
    router.post("/startWorkFlowAsync").handler(this::getRateAsync);
    router.post("/convertCurrency").handler(this::startConvertCurrency);
    router.post("/exChangeCurrency").handler(this::handleExchangeCurrency);

    //create server
    vertx.createHttpServer()
      .requestHandler(router)
      .listen(8080)
      .onSuccess(server -> System.out.println("http server verticle started success"))
      .onFailure(server -> System.out.println("http server verticle failed to run"));
  }

  private void greeting(RoutingContext rc) {
    JsonObject requestJson = rc.getBodyAsJson();
    String name = requestJson.getString("name");
    JsonObject result = new JsonObject();

    result.put("success", true).put("name", name);

    rc.response()
      .setStatusCode(200).putHeader("content-type", "application/json; charset=utf-8")
      .end(result.encodePrettily());
  }

  private void getRate(RoutingContext rc) {
    System.out.println("inside get rates");

    // This gRPC stubs wrapper talks to the local docker instance of the Temporal service.
    WorkflowServiceStubs service = WorkflowServiceStubs.newLocalServiceStubs();
    // WorkflowClient can be used to start, signal, query, cancel, and terminate Workflows.
    WorkflowClient client = WorkflowClient.newInstance(service);
    WorkflowOptions options = WorkflowOptions.newBuilder().setTaskQueue(Shared.CURRENCY_RATES_TASK_QUEUE).build();
    // WorkflowStubs enable calls to methods as if the Workflow object is local, but actually perform an RPC.
    TemporalHelloWoldWorkflow workflow = client.newWorkflowStub(TemporalHelloWoldWorkflow.class, options);
    // Synchronously execute the Workflow and wait for the response.
    Rates rates = new Rates();

    // rates = workflow.getCurrencyRates();

    ObjectMapper Obj = new ObjectMapper();
    String jsonStr;
    try {
      jsonStr = Obj.writeValueAsString(rates);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
    rc.response().setStatusCode(200).putHeader("content-type", "application/json; charset=utf-8").end(jsonStr);
  }

  private void getRateAsync(RoutingContext rc) {
    // This gRPC stubs wrapper talks to the local docker instance of the Temporal service.
    WorkflowServiceStubs service = WorkflowServiceStubs.newLocalServiceStubs();
    // WorkflowClient can be used to start, signal, query, cancel, and terminate Workflows.
    WorkflowClient client = WorkflowClient.newInstance(service);
    WorkflowOptions options = WorkflowOptions.newBuilder().setTaskQueue(Shared.CURRENCY_RATES_TASK_QUEUE).build();
    // WorkflowStubs enable calls to methods as if the Workflow object is local, but actually perform an RPC.
    TemporalHelloWoldWorkflow workflow = client.newWorkflowStub(TemporalHelloWoldWorkflow.class, options);
    // Asynchronous execution. This process will exit after making this call.
    // WorkflowExecution we = WorkflowClient.start(workflow::getCurrencyRates);

    rc.response().setStatusCode(200).putHeader("content-type", "application/json; charset=utf-8").end("{Message: success}");
  }

  private void startConvertCurrency(RoutingContext rc) {
    WorkflowServiceStubs service = WorkflowServiceStubs.newLocalServiceStubs();
    WorkflowClient client = WorkflowClient.newInstance(service);
    WorkflowOptions options = WorkflowOptions.newBuilder().setTaskQueue(Shared.CONVERT_CURRENCY_TASK_QUEUE).build();
    TemporalHelloWoldWorkflow workflow = client.newWorkflowStub(TemporalHelloWoldWorkflow.class, options);

   // WorkflowClient.start(workflow::startCurrencyChangeWorkFlow, 12);

    rc.response()
      .setStatusCode(200)
      .putHeader("content-type", "application/json; charset=utf-8")
      .end("{ Message : success }");
  }

  private void  handleExchangeCurrency(RoutingContext rc)
  {
    JsonObject jsonObject = rc.getBodyAsJson();
    vertx.eventBus().send(EventbusAddresses.CURRENCY_EXCHANGE_ADDRESS,jsonObject);

    JsonObject response = new JsonObject();
    response.put("Message","success");
    rc.response()
      .setStatusCode(200)
      .putHeader("content-type", "application/json; charset=utf-8")
      .end(response.encodePrettily());
  }
}
