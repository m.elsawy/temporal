package com.example.starter.vertx;

import com.example.starter.temporal.definitions.EventbusAddresses;
import com.example.starter.temporal.definitions.Shared;
import com.example.starter.temporal.workflows.TemporalHelloWoldWorkflow;
import io.temporal.client.WorkflowClient;
import io.temporal.client.WorkflowOptions;
import io.temporal.serviceclient.WorkflowServiceStubs;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

public class ExChangeCurrencyVerticle extends AbstractVerticle {
  @Override
  public void start(Promise<Void> startPromise) {
    vertx.eventBus()
      .consumer(EventbusAddresses.CURRENCY_EXCHANGE_ADDRESS)
      .handler(this::handleEvent);
  }

  private void handleEvent(Message<Object> objectMessage) {
    WorkflowServiceStubs service = WorkflowServiceStubs.newLocalServiceStubs();
    WorkflowClient client = WorkflowClient.newInstance(service);
    WorkflowOptions options = WorkflowOptions.newBuilder().setTaskQueue(Shared.CONVERT_CURRENCY_TASK_QUEUE).build();
    TemporalHelloWoldWorkflow workflow = client.newWorkflowStub(TemporalHelloWoldWorkflow.class, options);

    String messageBody = objectMessage.body().toString();
    JsonObject jsonObject = new JsonObject(messageBody);
    String from,to;
    from = jsonObject.getString("From");
    to = jsonObject.getString("To");
    Integer value = jsonObject.getInteger("Value");
    WorkflowClient.start(workflow::startCurrencyChangeWorkFlow, from, to, value);


  }

}
