package com.example.starter.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Verticle;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class MainVerticle extends AbstractVerticle {

  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    Verticle httpServerVerticle = new HttpServerVerticle();
    Verticle helloWorldWorkerVerticle = new HelloWorldWorkerVerticle();
    Verticle eventVerticleVerticle =  new EventBusVerticle();
    Verticle ExChangeCurrencyVerticle = new ExChangeCurrencyVerticle();

    vertx.deployVerticle(helloWorldWorkerVerticle);
    vertx.deployVerticle(httpServerVerticle);
    vertx.deployVerticle(eventVerticleVerticle);
    vertx.deployVerticle(ExChangeCurrencyVerticle);

    System.out.println("main verticle run success");
  }
}
