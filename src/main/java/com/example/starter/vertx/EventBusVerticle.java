package com.example.starter.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;

public class EventBusVerticle extends AbstractVerticle {
  @Override
  public void start(Promise<Void> startPromise){
    EventBus eb = vertx.eventBus();
    MessageConsumer<String> consumer  = eb.consumer("global.rates");
    consumer.handler(message -> {
      System.out.println("received a message " + message.body());
    });
    consumer.completionHandler(res -> {
      if (res.succeeded()) {
        System.out.println("handler run success");
      } else {
        System.out.println("Registration Filed");
      }
    });
    eb.publish("global.rates","hello from publish");
    eb.send("global.rates","hello form send");
  }
}
