package com.example.starter.vertx;

import com.example.starter.temporal.activities.impl.CurrencyRatesActivityImpl;
import com.example.starter.temporal.activities.impl.ExChangeCurrencyActivityImpl;
import com.example.starter.temporal.definitions.Shared;
import com.example.starter.temporal.workflows.impl.TemporalHelloWoldWorkflowImpl;
import io.temporal.client.ActivityCompletionClient;
import io.temporal.client.WorkflowClient;
import io.temporal.serviceclient.WorkflowServiceStubs;
import io.temporal.worker.Worker;
import io.temporal.worker.WorkerFactory;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;

public class HelloWorldWorkerVerticle extends AbstractVerticle {
  @Override
  public void start(Promise<Void> startPromise) {
    // This gRPC stubs wrapper talks to the local docker instance of the Temporal service.
    WorkflowServiceStubs service = WorkflowServiceStubs.newLocalServiceStubs();
    WorkflowClient client = WorkflowClient.newInstance(service);
    // Create a Worker factory that can be used to create Workers that poll specific Task Queues.
    WorkerFactory factory = WorkerFactory.newInstance(client);
    Worker worker = factory.newWorker(Shared.CONVERT_CURRENCY_TASK_QUEUE);
    // This Worker hosts both Workflow and Activity implementations.
    // Workflows are stateful, so you need to supply a type to create instances.
    worker.registerWorkflowImplementationTypes(TemporalHelloWoldWorkflowImpl.class);
    // Activities are stateless and thread safe, so a shared instance is used.
    ActivityCompletionClient completionClient = client.newActivityCompletionClient();
    worker.registerActivitiesImplementations(new CurrencyRatesActivityImpl(), new ExChangeCurrencyActivityImpl(completionClient, vertx));
    System.out.println("before start worker");
    // Start polling the Task Queue.
    factory.start();
  }
}
