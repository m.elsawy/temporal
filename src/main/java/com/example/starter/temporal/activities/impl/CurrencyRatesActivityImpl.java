package com.example.starter.temporal.activities.impl;

import com.example.starter.temporal.models.Rates;
import com.example.starter.temporal.activities.CurrencyRatesActivity;
import com.example.starter.utiles.HttpClient;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;

import java.io.IOException;
import java.net.URL;

public class CurrencyRatesActivityImpl implements CurrencyRatesActivity {
  @Override
  public Rates getRates() {

    try {
      String urlStr = "https://api.exchangerate.host/latest";
      URL parsedURL = new URL(urlStr);
      System.out.println(urlStr);
      Vertx vertx = Vertx.vertx();
      HttpRequest<JsonObject> httpRequest = HttpClient.get(vertx, urlStr, null);
      httpRequest.send().onSuccess(res -> {
        JsonObject jsonObject = res.bodyAsJsonObject().getJsonObject("AED");
        System.out.println(jsonObject.encodePrettily());
        System.out.println(res.statusMessage());
      }).onFailure(err -> System.out.println(err.getMessage()));
      return null;
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
  }
}
