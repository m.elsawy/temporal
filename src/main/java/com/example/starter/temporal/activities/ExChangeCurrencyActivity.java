package com.example.starter.temporal.activities;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;
import org.apache.kafka.common.protocol.types.Field;

@ActivityInterface
public interface ExChangeCurrencyActivity {
  @ActivityMethod
  String ExChangeCurrency(String from, String to, Integer value);
}
