package com.example.starter.temporal.activities.impl;

import com.example.starter.temporal.activities.CurrencyConverterActivity;
import com.example.starter.utiles.HttpClient;
import io.temporal.activity.Activity;
import io.temporal.activity.ActivityExecutionContext;
import io.temporal.client.ActivityCompletionClient;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;

public class CurrencyConverterActivityImpl implements CurrencyConverterActivity {

  private final ActivityCompletionClient completionClient;
  private final Vertx vertx;

  public CurrencyConverterActivityImpl(ActivityCompletionClient completionClient,Vertx vertx) {
    this.completionClient = completionClient;
    this.vertx = vertx;
  }

  @Override
  public String convertCurrency(Integer balance) {
    ActivityExecutionContext ctx = Activity.getExecutionContext();
    // Used to correlate reply
    byte[] taskToken = ctx.getInfo().getTaskToken();

    String urlStr = "https://api.exchangerate.host/latest";
    try {
      HttpRequest<JsonObject> httpRequest = HttpClient.get(vertx, urlStr, null);
      httpRequest.send()
        .onSuccess(res -> {
          JsonObject jsonObject = res.body().getJsonObject("rates");
          Integer amount = balance * jsonObject.getInteger("AED");
          // Complete our workflow activity using ActivityCompletionClient
          completionClient.complete(taskToken, amount.toString());
        })
        .onFailure(err ->
          {
            System.out.println(err.getMessage());
            completionClient.completeExceptionally(taskToken, new Exception("failed connect to rates api"));
          }
        );
    } catch (Exception e) {
      completionClient.completeExceptionally(taskToken, e);
    }
    ctx.doNotCompleteOnReturn();
    return "ignored";
  }
}
