package com.example.starter.temporal.activities.impl;

import com.example.starter.temporal.activities.ExChangeCurrencyActivity;
import com.example.starter.utiles.HttpClient;
import io.temporal.activity.Activity;
import io.temporal.activity.ActivityExecutionContext;
import io.temporal.client.ActivityCompletionClient;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;

import java.io.IOException;
import java.net.URL;

public class ExChangeCurrencyActivityImpl implements ExChangeCurrencyActivity {
  private final ActivityCompletionClient completionClient;
  private final Vertx vertx;

  public ExChangeCurrencyActivityImpl(ActivityCompletionClient completionClient, Vertx vertx) {
    this.completionClient = completionClient;
    this.vertx = vertx;
  }

  @Override
  public String ExChangeCurrency(String from, String to, Integer value) {
    ActivityExecutionContext ctx = Activity.getExecutionContext();
    byte[] taskToken = ctx.getInfo().getTaskToken();
    try {
      String urlStr = "https://api.apilayer.com/currency_data/convert?to=" + to + "&from=" + from + "&amount=" + value + "";
      HttpRequest<JsonObject> httpRequest = HttpClient.get(vertx, urlStr, null)
        .putHeader("apikey", "zkG80pM8aerP3I3PmTxl55yIiMcbSQpj");
      httpRequest.send().onSuccess(res -> {
          Double result = res.body().getDouble("result");
          //System.out.println(result.toString());
          System.out.println(res.statusMessage()); // Complete our workflow activity using ActivityCompletionClient
          completionClient.complete(taskToken, result);
        })
        .onFailure(err ->
          {
            System.out.println(err.getMessage());
            completionClient.completeExceptionally(taskToken, new Exception("failed connect to exchange currency api"));
          }
        );
    } catch (Exception e) {
      completionClient.completeExceptionally(taskToken, e);
    }
    ctx.doNotCompleteOnReturn();
    return "ignored";
  }
}
