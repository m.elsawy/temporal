package com.example.starter.temporal.activities;

import com.example.starter.temporal.models.Rates;
import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;

import java.io.IOException;

@ActivityInterface
public interface CurrencyRatesActivity {
  @ActivityMethod
  Rates getRates();
}
