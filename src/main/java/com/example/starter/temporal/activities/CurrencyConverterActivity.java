package com.example.starter.temporal.activities;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;

@ActivityInterface
public interface CurrencyConverterActivity {
  @ActivityMethod
  String convertCurrency(Integer balance);
}
