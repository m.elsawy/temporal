package com.example.starter.temporal.workflows.impl;

import com.example.starter.temporal.activities.CurrencyConverterActivity;
import com.example.starter.temporal.activities.ExChangeCurrencyActivity;
import com.example.starter.temporal.workflows.TemporalHelloWoldWorkflow;
import io.temporal.activity.ActivityOptions;
import io.temporal.common.RetryOptions;
import io.temporal.workflow.Workflow;

import java.time.Duration;

public class TemporalHelloWoldWorkflowImpl implements TemporalHelloWoldWorkflow {
  ActivityOptions options = ActivityOptions
    .newBuilder()
    .setScheduleToCloseTimeout(Duration.ofSeconds(15))
    .setRetryOptions(
      RetryOptions.newBuilder()
        .setMaximumAttempts(10)
        .setInitialInterval(Duration.ofSeconds(1))
        .setMaximumInterval(Duration.ofSeconds(10))
        .build()
    )
    .build();
  private final ExChangeCurrencyActivity exChangeCurrencyActivity = Workflow.newActivityStub(ExChangeCurrencyActivity.class, options);

  @Override
  public String startCurrencyChangeWorkFlow(String from, String to, Integer value) {
    exChangeCurrencyActivity.ExChangeCurrency(from, to, value);
    return "return from Currency Change WorkFlow";
  }
}
