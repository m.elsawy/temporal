package com.example.starter.temporal.workflows;

import com.example.starter.temporal.models.Rates;
import io.temporal.workflow.WorkflowInterface;
import io.temporal.workflow.WorkflowMethod;
import org.apache.kafka.common.protocol.types.Field;

import java.io.IOException;

@WorkflowInterface
public interface TemporalHelloWoldWorkflow {
  @WorkflowMethod
    //define entry point to workflow
  String startCurrencyChangeWorkFlow(String from, String to, Integer value);
}
