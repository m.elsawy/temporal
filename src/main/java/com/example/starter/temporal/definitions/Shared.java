package com.example.starter.temporal.definitions;

public interface Shared {
  String CURRENCY_RATES_TASK_QUEUE = "CURRENCY_RATES_TASK_QUEUE";
  String CONVERT_CURRENCY_TASK_QUEUE = "CONVERT_CURRENCY_TASK_QUEUE";
}
