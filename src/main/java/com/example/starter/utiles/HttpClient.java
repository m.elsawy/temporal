package com.example.starter.utiles;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.codec.BodyCodec;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

public class HttpClient {
  public static HttpRequest<JsonObject> get(Vertx vertx, String url, HashMap<String, String> headers) {
    try {
      URL parsedURL = new URL(url);
      HttpRequest<JsonObject> client = WebClient
        .create(vertx)
        .get(parsedURL.getDefaultPort(), parsedURL.getHost(), parsedURL.getFile())
        .as(BodyCodec.jsonObject())
        .ssl(true)
        .timeout(15000);
      if (headers != null) {
        headers.forEach(client::putHeader);
      }
      return client;
    } catch (MalformedURLException e) {
      e.printStackTrace();
      return null;
    }
  }
}
